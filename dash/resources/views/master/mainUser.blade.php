<div class="modal fade" id="modal-add">
    <div class="modal-dialog" style="width:800px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"> افزودن مورد جدید <span id="addUserMT"></span></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div  class="box-body addNewUserBox" >
                        <div  class="box-body">

                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group col-sm-12">
                                        <label class="control-label "  style="padding:2px"><span id="nameLable">تاریخ</span><span style="color:#FF0000">*</span></label>
                                        <input type="text" id="a_date"  class="form-control pull-right" readonly >                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group col-sm-12">
                                        <label class="control-label "  style="padding:2px">عنوان</label>
                                        <input class="form-control"  name="title"  id="title"   type="text" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group col-sm-12">
                                        <label class="control-label "  style="padding:2px">متن</label>
                                        <input type="radio" class="radio" onchange="aType(this.value)" checked id="a_type"  value="0"  name="a_type" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group col-sm-12">
                                        <label class="control-label "  style="padding:2px">تصویر</label>
                                        <input class="radio" type="radio" onchange="aType(this.value)" id="a_type" value="1"  name="a_type" >
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="textField">
                                <div class="col-md-12">
                                    <div class="form-group col-sm-12">
                                        <label class="control-label "  style="padding:2px">متن <span style="color:#FF0000">*</span></label>
                                        <textarea   class="form-control"  name="text"  id="text"></textarea>

                                    </div>
                                </div>
                            </div>
                            <div  id="imageFiled" style="display: none">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <div class="input-group input-group-file">
                                        <label class="input-group-btn">
                                        <span class="btn btn-primary ">
                                            آپلود تصویر jpeg,png,jpg
                                        </span>
                                        </label>
                                        <input type="text" class="form-control"  title="input" readonly>
                                    </div><!-- /.input-group -->
                                </div><!-- /.col-x-x -->
                                <div class="col-xs-6 col-sm-3">
                                    <button id="addNewFile" type="button" class="btn btn-primary waves-effect">جدید</button>
                                </div>
                            </div>
                            <div class="row" id="fileDiv" style="padding-right: 15px"></div>
                        </div>
                        </div>
                        <div class="modal-footer" align="center" id="alertMsg" style="color: red">

                        </div>
                    </div>
                    <!-- /.box-body -->

                    <!-- /.box-footer -->

                </form>
            </div>
            <div class="modal-footer addNewUserBox" style="height:90px" >
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">بستن</button>
                <button type="button" class="btn btn-primary" id="register">ذخیره</button>
                <span style="display: none" class="control-label" id="waitAddUser">لطفا کمی صبر کنید</span>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<ul class="timeline timeline-inverse">
    <!-- timeline time label -->
    <li class="time-label">
                  <span class="bg-red">
                   {{$date}}
                  </span>
    </li>
    <!-- /.timeline-label -->
    <!-- timeline item -->
    @foreach($activities as $activity)
        <li>
            <i class="fa fa-user bg-blue"></i>

            <div class="timeline-item">
                <span class="time"><i class="fa fa-calendar"></i> {{\App\Helpers\Fn::toJDate($activity->a_date)}}</span>

                <h3 class="timeline-header">{{$activity->title}}</h3>

                <div class="timeline-body">
                    @if($activity->a_type == 0)
                        {{$activity->text}}
                        @else
                        <div class="row">
                            @foreach($activity->attachments as $attachment)


                                <div class="col-md-3">
                                    <div class="form-group col-sm-12">
                                        
                                    <img  data-toggle="modal" data-target="#modal-showImage" style="cursor: pointer" onclick="$('#imageShow').attr('src','{{asset($attachment->url)}}');" src="{{asset($attachment->url)}}"  width="100%">
                                    </div>
                                </div>

                                @endforeach
                        </div>
                    @endif
                </div>
                <div class="timeline-footer">

                </div>
            </div>
        </li>
        @endforeach

    <!-- END timeline item -->

    <li>
        <i class="fa fa-clock-o bg-gray"></i>
    </li>
</ul>
<div class="modal fade" id="modal-showImage">
    <div class="modal-dialog" style="width:600px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">  <span id="addUserMT"></span></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div  class="box-body addNewUserBox" >
                        <div  class="box-body">

<img src="" id="imageShow" width="100%">
                        </div>

                    </div>
                    <!-- /.box-body -->

                    <!-- /.box-footer -->

                </form>
            </div>
            <div class="modal-footer" style="height:90px" >
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<input type="hidden" value="1" id="fileC" name="fileC">
<script>
    var j = 1;
    $('#a_date, #a_date').MdPersianDateTimePicker({TargetSelector: '#a_date',FromDate:true,EnglishNumber:true});
//    $(function () {
//        CKEDITOR.replace('text');
//        $('.textarea').wysihtml5();
//    });
    function aType(a_type) {
        if (a_type == '1'){
            $('#textField').fadeOut(300,function () {
                $('#imageFiled').fadeIn(300);
            });
        }
        else {
            $('#imageFiled').fadeOut(300,function () {
                $('#textField').fadeIn(300);
            });
        }
    }
    function addBtn() {
        $('#addNewFile').css('display','block');
    }
    $('#addNewFile').click(function () {
        $('#addNewFile').css('display','none');
        $('#fileC').val(j);
        var div = '<br><div class="row" ><div class="col-xs-12 col-sm-4 col-md-4"><div class="input-group input-group-file"><label class="input-group-btn"><span class="btn btn-primary btnFileInputGroup"> <input type="file" name="file'+j+'" id="file'+j+'" accept="image/jpg, image/jpeg, image/png" onchange="addBtn();uploadFile('+"'image"+j+"'"+',this)" style="display: block;"></span></label><input type="text"  class="form-control" title="input" readonly></div></div></div>';
        div += '<input id="image'+j+'Base" value="" type="hidden">';
        j++;
        $("#fileDiv").append(div);
    });
    $('#register').click(function () {
        $('#register').prop('disabled', true);

        title      	= $('#title').val();
        a_date     	= $('#a_date').val();
        //a_text    = CKEDITOR.instances['text'].getData();
        a_text      = $('#text').val();
        fileC   	= $('#fileC').val();
        a_type   	= $('#a_type:checked').val();

        if ( title == ''  || a_date == ''  ){

            $('#alertMsg').html('لطفا همه فیلد ها را کامل کنید');
            $('#register').prop('disabled', false);

            return false;
        }

        if (a_type == '1'){
            for(i=1;i<=fileC;i++){
                var file = $('#file'+i).val();
                if ( file != '' ) {
                    var ext = file.split('.').pop();

                    if ( $('#file'+i)[0].files[0].size > 1048576 ){

                        $('#alertMsg').html('حجم فایل تصویر بیش از 1 مگابایت است');
                        $('#register').prop('disabled', false);

                        return false;
                    }
                    else if ( !( ext == 'png' || ext == 'PNG' || ext == 'jpeg' || ext == 'JPEG' || ext == 'jpg' || ext == 'JPG')  ) {
                        $('#alertMsg').html('نوع فایل تصویر  صحیح نیست');
                        $('#register').prop('disabled', false);
                        return false;
                    }

                }
            }
        }



        var form_data = new FormData();

        $('#register').text( 'لطفا کمی صبر کنید');

        count = 0;
        if (a_type == '1'){
            if (fileC > 0){
                for(i=0;i<fileC;i++){
                    k = i+1;
                    img = $('#image'+k+'Base').val();
                    if (img.length > 10){
                        form_data.append('image'+i, img);
                        count++;
                    }

                }

            }
        }




            form_data.append('user_id', '{{$user->id}}');
            form_data.append('count', count);
            form_data.append('title', title);
            form_data.append('a_date', a_date);
            form_data.append('a_text', a_text);
            form_data.append('a_type', a_type);
            $('#register').text( 'لطفا کمی صبر کنید');
            $('#cansel').prop('disabled', true);
            $.ajax({
                url: 'http://localhost:8000/api/registerAct',
                data: form_data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success :function(data){
                    if (data.status ){
                        $('#modal-add').modal('hide');
                        $('#successAmg').html('مورد جدید با موفقیت ثبت شد');
                        $('#modal-success').modal('show');

                        postMenus('');


                    }
                    else {
                        alert('خطا در ارتباط با سرور');
                    }


                    //$('#bg').fadeOut(100);

                }});




    });
</script>