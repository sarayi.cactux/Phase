<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-right image">
                <img src="{{asset($user->img)}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-right info">
                <p>{{$user->name.' '.$user->family}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> آنلاین</a>
            </div>
        </div>
        <!-- search form -->
        <div align="center" style="padding-top: 40px;">
            <button type="button"  class="btn-lg btn-info" data-toggle="modal" data-target="#modal-add" >&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-plus"></i> افزودن &nbsp;&nbsp;&nbsp;&nbsp;</button>

        </div>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->

    </section>
    <input type="hidden" id="menuURL">
    <!-- /.sidebar -->
</aside>