<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author-design" content="Sarayi">
    <meta name="author" content="Mohammad Sarayi">
    <title> کنترل پنل مدیریت</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('panel/dist/css/bootstrap-theme.css')}}">
    <!-- Bootstrap rtl -->
    <link rel="stylesheet" href="{{asset('panel/dist/css/rtl.css')}}">
    <!-- persian Date Picker -->
    <link rel="stylesheet" href="{{asset('panel/dist/css/persian-datepicker-0.4.5.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('panel/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('panel/bower_components/Ionicons/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('panel/bower_components/select2/dist/css/select2.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('panel/dist/css/AdminLTE.css')}}">
    <link rel="stylesheet" href="{{asset('css/simpletree.css')}}">

    <link rel="stylesheet" href="{{asset('panel/Content/MdBootstrapPersianDateTimePicker/jquery.Bootstrap-PersianDateTimePicker.css')}}" />


    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('panel/dist/css/skins/_all-skins.min.css')}}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{asset('panel/bower_components/morris.js/morris.css')}}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{asset('panel/bower_components/jvectormap/jquery-jvectormap.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset('panel/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{asset('panel/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onload="chengeURL()" onhashchange="chengeURL()">
<input type="hidden" value="{{csrf_token()}}" id="_token">
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">پنل</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>کنترل پنل مدیریت</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>


            <!-- Delete This after download -->
            <!-- End Delete-->



            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{asset($user->img)}}" class="user-image" alt="User Image">
                            <span class="hidden-xs">{{  $user->name.' '.$user->family }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="{{asset($user->img) }}" style="cursor: pointer" data-toggle="modal" data-target="#modal-changeImage" class="img-circle" alt="User Image">

                                <p>
                                    <small>@if($user->id = '1')
                                        مدیریت کل سایت
                                    @endif</small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body">

                                <!-- /.row -->
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a href="#" class="btn btn-default btn-flat" data-toggle="modal" data-target="#modal-changePass">تغییر رمز ورود</a>
                                </div>
                                <div class="pull-left">
                                    <a href="{{action('userController@logOut')}}" class="btn btn-default btn-flat">خروج</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    <li>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- right side column. contains the logo and sidebar -->
@yield('menu');
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                داشبرد
                <small>کنترل پنل</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
                <li class="active">داشبرد</li>
            </ol>
        </section>

        <!-- Main content -->
        @yield('content')

        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer text-left">
        <strong>Copyleft &copy; 2017-2018 <a href="https://localhost">فاز اول</a> </strong>
    </footer>


    <div class="modal fade" id="modal-changePass">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">تغییر رمز ورود</h4>
                </div>
                <div class="modal-body">

                    <form class="form-horizontal">
                        <div class="box-body" id="changePassBox">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">رمز قبلی</label>

                                <div class="col-sm-10">
                                    <input type="password" class="form-control" id="oldPass" placeholder="رمز عبور قبلی">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">رمز جدید</label>

                                <div class="col-sm-10">
                                    <input type="password" class="form-control" id="newPass" placeholder="رمز عبور جدید">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">تکرار </label>

                                <div class="col-sm-10">
                                    <input type="password" class="form-control" id="conNewPass" placeholder="تکرار رمز عبور جدید">
                                </div>
                            </div>
                            <div class="modal-footer" align="center" id="alertMsg" style="color: red">

                            </div>
                        </div>
                        <!-- /.box-body -->

                        <!-- /.box-footer -->

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">بستن</button>
                    <button type="button" class="btn btn-primary" id="changePass">ذخیره</button>
                    <span style="display: none" class="control-label" id="wait">لطفا کمر صبر کنید</span>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modal-changeImage">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">تغییر تصویر پروفایل</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <div class="box-body" id="changeImageBox">
                            <div class="form-group">
                                <label for="profileImage">ارسال تصویر جدید</label>

                                <input type="file" id="profileImage" accept=".jpg, .jpeg, .png">

                                <p class="help-block">حجم فایل نباید بیش از 2 مگابایت باشد. پسوند فایل باید jpeg,png,jpg باشد</p>
                            </div>
                            <div class="modal-footer" align="center" id="alertMsgImage" style="color: red">

                            </div>
                        </div>
                        <!-- /.box-body -->

                        <!-- /.box-footer -->

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">بستن</button>
                    <button type="button" class="btn btn-primary" id="changeImage">ذخیره</button>
                    <span style="display: none" class="control-label" id="waitImage">لطفا کمر صبر کنید</span>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

</div>
<!-- ./wrapper -->
<div id="loading" style="display: none; position: fixed; top: 80px; left: 41%; width:13%; height: 40px; background: #fff0ff; padding: 6px">
    <h4 class="modal-title">در حال دریافت اطلاعات</h4>
</div>
<div class="modal modal-success fade" id="modal-success">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" id="successAmg">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">خروج</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- jQuery 3 -->
<script src="{{asset('panel/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('panel/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{asset('panel/bower_components/select2/dist/js/select2.full.min.js')}}"></script>

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('panel/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- Morris.js charts -->
<script src="{{asset('panel/bower_components/raphael/raphael.min.js')}}"></script>
<script src="{{asset('panel/bower_components/morris.js/morris.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('panel/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('panel/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('panel/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('panel/bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('panel/bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{asset('panel/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('panel/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('panel/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('panel/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- iCheck 1.0.1 -->
<script src="{{asset('panel/plugins/iCheck/icheck.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('panel/bower_components/fastclick/lib/fastclick.js')}}"></script>
<script src="{{asset('js/func.js')}}"></script>
<script src="{{asset('js/simpletreemenu.js')}}"></script>
<script src="{{asset('js/jalaali.js')}}"></script>
<script src="{{asset('js/jquery.Bootstrap-PersianDateTimePicker.js')}}"></script>

<!-- AdminLTE App -->
<script src="{{asset('panel/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('panel/dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('panel/dist/js/demo.js')}}"></script>
<script src="{{asset('panel/dist/js/persian-date-0.1.8.min.js')}}"></script>
<script src="{{asset('panel/dist/js/persian-datepicker-0.4.5.min.js')}}"></script>
<!-- InputMask -->
<script src="{{asset('panel/plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{asset('panel/plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
<script src="{{asset('panel/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>
<script src="{{asset('panel/bower_components/ckeditor/ckeditor.js')}}"></script>

<script language="javascript" type="text/javascript">

    $(document).ready(function(){
        $('.select2').select2();
        var lastLink ;

        $(".menus").click(function() {
            lastLink = $(this).attr("href");
            $('#menuURL').val(lastLink);
            postMenus(lastLink);



        });

    });
    function chengeURL() {

           var url = location.hash;


           if ( $('#menuURL').val() == '' ){
               postMenus(url);
           }
           else {
               $('#menuURL').val('') ;
           }




    }
    function postMenus(url){

        $('#loading').fadeIn(100);
        url = url.substr(1);
        $.post(url, {
                _token          : $('#_token').val(),
            },
            function(data){

                if ( data ){
                    $('#panelContent').html(data);
                    $('#loading').fadeOut(100);
                }//  2pm
            });
    }
</script>
<script language="JavaScript">
    $('#changePass').click(function () {
        oldPass = $('#oldPass').val();
        newPass = $('#newPass').val();
        newCon  = $('#conNewPass').val();
        if ( newPass == ''  || oldPass == '' || newCon == ''){

            $('#alertMsg').html('لطفا همه فیلد ها را پر کنید');
            return false;
        }
        if (newPass == newCon){
            $('#alertMsg').html('');
            $('#wait').fadeIn(100);
            $('#changePass').prop('disabled', true);
            $.post("/changePass", {
                    oldPass         : oldPass,
                    newPass         : newPass,
                    _token          : $('#_token').val(),
                },
                function(data){
                    if ( data.status ){
                        $('#changePassBox').html('رمز عبور با موفقیت به روز رسانی شد');
                        $('#wait').fadeOut(100);
                        $('#changePass').fadeOut(100);

                    }//  2pm
                    else {
                        msg = data.error;
                        $('#alertMsg').html(msg);
                        $('#wait').fadeOut(100);
                        $('#changePass').prop('disabled', false);

                    }
                    //$('#bg').fadeOut(100);

                }, "json");
        }
        else {

            $('#alertMsg').html('رمز جدید و تکرار آن، برابر نیستند');
        }
    });
    $('#changeImage').click(function () {


        var file = $('#profileImage').val();
        var ext = file.split('.').pop();
        if ( file == '' ){

            $('#alertMsgImage').html('فایل انتخاب نشده');
            return false;
        }
        if ( $("#profileImage")[0].files[0].size > 2097152 ){

            $('#alertMsgImage').html('حجم فایل بیش از 2 مگابایت است');
            return false;
        }
        if ( ext == 'png' || ext == 'PNG' || ext == 'jpeg' || ext == 'JPEG' || ext == 'jpg' || ext == 'JPG'  ) {


            var file_data = $('#profileImage').prop('files')[0];
            var token = $('#_token').val();
            var form_data = new FormData();
            form_data.append('authorpic', file_data);
            form_data.append('_token', token);
            $.ajax({
                url: '/changeImage',
                data: form_data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success :function(data){
                    if ( data.status ){
                        $('#changeImageBox').html('تصویر با موفقیت به روز رسانی شد');
                        $('#waitImage').fadeOut(100);
                        $('#changeImage').fadeOut(100);

                    }//  2pm
                    else {
                        msg = data.error;
                        $('#alertMsgImage').html(msg);
                        $('#waitImage').fadeOut(100);
                        $('#changeImage').prop('disabled', false);

                    }
                    //$('#bg').fadeOut(100);

                }});
            $('#alertMsgImage').html('');
            $('#waitImage').fadeIn(100);
            $('#changeImage').prop('disabled', true);

        }
        else {
            $('#alertMsgImage').html('پسوند فایل صحیح نیست');
            return false;
        }


    });
    function uploadFile(inputId,input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#'+inputId+'Base').val(e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

</script>
<div id="reload" style="display:none;"></div>
<div id="uploading" class="uploading"></div>

</body>
</html>
