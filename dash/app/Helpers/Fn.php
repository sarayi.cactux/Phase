<?php

namespace App\Helpers;


use Verta;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use File;

class Fn
{
    //
	public static  function fn($m)
{
	$p = "";
	$startpos = 0;
	$endpos = -1;
	
	while (true)
	{
		if ($endpos == -1)
			$startpos = strpos($m, "<");
		else
			$startpos = strpos($m, "<", $endpos);
		if ($startpos === false)
		{
			$startpos = strlen($m);
			$p .= self::str2fn(substr($m, $endpos + 1, $startpos - $endpos - 1));
			break;
		}
		$p .= self::str2fn(substr($m, $endpos+1, $startpos - $endpos -1));
		$endpos = strpos($m, ">", $startpos);
		$p .= substr($m, $startpos , $endpos - $startpos + 1);
	}
	return $p;
}

public static  function str2fn($m)
{
	$m=str_replace("1","۱",$m);
	$m=str_replace("2","۲",$m);
	$m=str_replace("3","۳",$m);
	$m=str_replace("4","۴",$m);
	$m=str_replace("5","۵",$m);
	$m=str_replace("6","۶",$m);
	$m=str_replace("7","۷",$m);
	$m=str_replace("8","۸",$m);
	$m=str_replace("9","۹",$m);
	$m=str_replace("0","۰",$m);
	return $m;
}

public static  function reverseFN($m)
{
	$m=str_replace("۱","1",$m);
	$m=str_replace("۲","2",$m);
	$m=str_replace("۳","3",$m);
	$m=str_replace("۴","4",$m);
	$m=str_replace("۵","5",$m);
	$m=str_replace("۶","6",$m);
	$m=str_replace("۷","7",$m);
	$m=str_replace("۸","8",$m);
	$m=str_replace("۹","9",$m);
	$m=str_replace("۰","0",$m);
	return $m;

}
 

public static  function echo_date($intDate){
	if ( strlen($intDate) == 8 ){
  	$year  = substr($intDate,0,4);
	$month = substr($intDate,4,2);
	$day   = substr($intDate,6,2);
	return self::fn($year.'/'.$month.'/'.$day);
	}
	else if ( strlen($intDate) == 6 ){
  	$year  = substr($intDate,0,4);
	$month = substr($intDate,4,1);
	$day   = substr($intDate,5,1);
	return self::fn($year.'/0'.$month.'/0'.$day);
	}
	else if ( strlen($intDate) == 7 ){
  	$year  = substr($intDate,0,4);
	$month = substr($intDate,4,1);
	$day   = substr($intDate,5,2);
	
	return ($year.'/0'.$month.'/'.$day);
	}
}
public static  function echo_date2($intDate){
  	$year  = substr($intDate,0,4);
	$month = substr($intDate,4,2);
	$day   = substr($intDate,6,2);
	return ($year.'/'.$month.'/'.$day);
}
public static  function echoNum($num){
        $arr = str_split($num);
        $snum = '';
        $j    = 1;
        for ( $i=count($arr)-1;$i>=0;$i-- ) {
            if ( $j != 4 ){
                $snum = $arr[$i].$snum;
                $j++;
            }
            else {
                $snum = $arr[$i].','.$snum;
                $j=2;
            }
        }
        return self::fn($snum);
    }
    public static function echoDate($date){
        $date = Verta::instance($date);
        return substr($date,0,10);
    }
    public static function toGDate($date){

        $date = str_replace('/','-',$date);
        $date = explode('-',$date,3);

        $date = Verta::getGregorian($date[0],$date[1],$date[2]);

        if($date[1] < 10){
            $date[1] = '0'.$date[1];
        }
        if($date[2] < 10){
            $date[2] = '0'.$date[2];
        }
        return $date[0]."-".$date[1]."-".$date[2]." 00:00:00";
    }
    public static function toJDate($date){
        if(strlen($date) < 10 ) return '';
        $arr = explode($date,'-',3);
        $year = substr($date,0,4);
        $mount= substr($date,5,2);
        $day  = substr($date,8,2);
        $gDate = Verta::getJalali($year,$mount,$day);

        return $gDate[0].'/'.$gDate[1].'/'.$gDate[2];
    }
    public static function remotefileSize($url) {
       // $size = File::size($url);
        if (strlen($url) < 5){
            return 0;
        }
//        $file = str_replace('https://hamkelasi.ir','',$url);
//        $size = File::size($_SERVER['DOCUMENT_ROOT'] .$file);
//        $size = round($size/1024/1024,2);
//        return $size;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
        curl_exec($ch);
        $filesize = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
        curl_close($ch);
        if ($filesize) return round($filesize/1024/1024,2);
    }

}

