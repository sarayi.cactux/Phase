<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = "activites";
    public function attachments()
    {
        return $this->morphMany('App\Models\Attachment', 'attachment');
    }
}
