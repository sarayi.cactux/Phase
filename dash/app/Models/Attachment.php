<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{

    protected $fillable = [ ];
    protected $guarded = ['id','attachment_id','attachment_type','updated_at','created_at'];
    public function attachment()
    {
        return $this->morphTo();
    }
}
