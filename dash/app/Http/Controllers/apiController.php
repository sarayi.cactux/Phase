<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Attachment;
use App\Models\Activity;
use App\Helpers\Fn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

header('Access-Control-Allow-Origin: *');

class apiController extends Controller
{
    public function checkUsername($username){
            $user = User::where('username',$username)->exists();
            if ($user){
                $status = true;
            }
            else {
                $status = false;
            }
            return ['status'=>$status];
    }
    public function register(Request $request){

        $name       = $request->input('name');
        $family     = $request->input('family');
        $age        = $request->input('age');
        $gender     = $request->input('gender');
        $password   = $request->input('password');
        $username   = $request->input('username');
        $image      = $request->input('image');
        $user = User::where('username',$username)->exists();
        if ($user){
            return ['status'=>false];
        }
        $data = $image;
        $png_url = time().rand(100000,9999999).".png";

        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        $data = base64_decode($data);

        file_put_contents('users/'.$png_url, $data);
        $imageUrl = 'users/'.$png_url;
        $password = Hash::make($password);
        $user = new User();
        $user->username = $username;
        $user->password = $password;
        $user->name     = $name;
        $user->family   = $family;
        $user->img      = $imageUrl;
        $user->age      = $age;
        $user->gender   = $gender;
        if ($user->save()){

            $status = true;
        }
        else {
            $status = false;
        }
        return ['status'=>$status];

    }
    public function checkLogin(Request $request)
    {

        $u = $request->input('username');
        $p = $request->input('password');

        $user = User::where('username', '=', $u)->first();
        $userInf = '';
        if (!$user || !Auth::attempt(['username' => $u, 'password' => $p])) {

            $status = false;
        }
        else {
            $status = true;
            $userInf = $user;

        }
        return ['status'=>$status,'user'=>$userInf];


    }
    public function registerAct(Request $request){
        $status = false;
            $title      = $request->input('title');
            $a_date     = Fn::toGDate($request->input('a_date'));
            $a_type     = intval($request->input('a_type'));
            $text       = $request->input('a_text');
            $user_id    = $request->input('user_id');
            $count      = intval($request->input('count'));
            if ($a_type == 1){
                $text = '';

            }
            $activity = new Activity();
            $activity->user_id  = $user_id;
            $activity->title    = $title;
            $activity->a_date   = $a_date;
            $activity->a_type   = $a_type;
            $activity->text     = $text;

            if ( $activity->save()){
                if ($a_type == 1){
                    for ($i=0;$i<$count;$i++){
                        $data = $request->input('image'.$i);
                        $png_url = rand(100000,9999999).time().".png";

                        list($type, $data) = explode(';', $data);
                        list(, $data)      = explode(',', $data);
                        $data = base64_decode($data);

                        file_put_contents('users/'.$png_url, $data);
                        $imageUrl = 'users/'.$png_url;
                        $attach = new Attachment(['url' => $imageUrl]);
                        $activity->attachments()->save($attach);

                    }
                }
                $status = true;
            }

        return ['status'=>$status];

    }
    public function userAct($userId){
        $user = User::find($userId);
        $activities = '';
        $status   = false;
        if ($user){
            $status = true;
            $activities = Activity::where('user_id',$user->id)->orderBy('created_at','DESC')->limit(100)->with('attachments')->get();

        }
        return ['status'=>$status,'activities'=>$activities];
    }

}
