<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Hekmatinasser\Verta\Verta;

class userController extends Controller
{
    public function userAuth(){

        if ( Auth::user()){
            $user = Auth::user();
            return redirect(url('/user'));
        }
        else {
            return view('user.login');
        }
    }
    public function checkLogin(Request $request)
    {
        $this->validate($request, ['captcha' => 'required|captcha']);

        $u = $request->input('username');
        $p = $request->input('password');

        $user = User::where('username', '=', $u)->first();
        if (!$user || !Auth::attempt(['username' => $u, 'password' => $p])) {
            $error = 'نام کاربری و رمز عبور صحیح نیست';
            return view('user.login',compact('error'));
        }
        else {
            return redirect(url('/user'));        }


    }
    public function user(){
        $user = Auth::user();
        return view('master.user',compact('user'));
    }
    public function mainUser(){
        $user = Auth::user();
        $activities = Activity::where('user_id',$user->id)->orderBy('created_at','DESC')->limit(100)->with('attachments')->get();
        $date = substr(Verta::now(),0,10);
        return view('master.mainUser',compact('activities','user','date'));
    }
    public function logOut(){
        Auth::logout();
        return redirect(url('http://localhost'));

    }
}
