<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::any('/checkUsername/{username}', 'apiController@checkUsername');
Route::any('/register', 'apiController@register');
Route::any('/login', 'apiController@checkLogin');
Route::any('/registerAct', 'apiController@registerAct');
Route::any('/userActs/{userId}', 'apiController@userAct');
