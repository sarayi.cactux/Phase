<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/login', 'userController@userAuth');
Route::post('/login', 'userController@checkLogin');
Route::get('/logOut', 'userController@logOut');
Route::middleware(['auth'])
    ->prefix('/user')
    ->group(function () {

        Route::get('/', 'userController@user');
        Route::post('/','userController@mainUser');








    });